/**
 * View Models used by Spring MVC REST controllers.
 */
package com.dattt.team_management.web.rest.vm;
